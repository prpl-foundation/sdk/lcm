# prpl LCM SDK



## Getting started

Introducing the LCM Standalone SDK, designed specifically for generating LCM compatible containers.

To make use of the SDK for creating LCM compatible containers, it is necessary to have Docker support 
enabled on your host system. 
You can refer to the [comprehensive SDK documentation](https://prplfoundationcloud.atlassian.net/wiki/spaces/LCM/pages/194936927/LCM+SDK+-+Introduction+and+howto) for further details.

Here's a step-by-step guide to installing and running the SDK for generating the default SDK image.

Install and run the SDK:
```
docker login -u="${GITLAB_USERNAME}" -p="${GITLAB_PASSWORD}" registry.gitlab.com
docker run -it --name lcm_sdk registry.gitlab.com/prpl-foundation/sdk/lcm/lcm_sdk_cortexa53:v1.0.0
```

To initiate the creation of an initial default LCM image using the SDK, begin by connecting to the SDK container and commencing the image building process.
```
docker exec -it lcm_sdk /bin/bash
devtool build-image image-lcm-container-minimal
```

Embrace the possibilities with the LCM Standalone SDK and unlock the potential of LCM compatible containers.
